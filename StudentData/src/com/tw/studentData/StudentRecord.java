package com.tw.studentData;

import java.util.Hashtable;
import java.util.Map;

public class StudentRecord {

    private static final Map<Integer, Student> studentData = new Hashtable<>();

    public void addStudent(int roll, Student student) {
        if (studentData.containsValue(student))
            System.out.println("existed already");
        studentData.put(roll, student);
    }

    public void removeStudent(int rollNo) {
        if (studentData.containsKey(rollNo))
            studentData.remove(rollNo);
        else
            System.out.println("No key found");
    }

    public void updateStudent(int rollNo, Student student) {
        if (studentData.containsKey(rollNo))
            studentData.replace(rollNo, student);
        else
            System.out.println("No key found");
    }

    public Map<Integer, Student> getStudentData() {
        return studentData;
    }
}
