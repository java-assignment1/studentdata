package com.tw.studentData;

public class Student {

    private final String name;
    private final int marks;
    private final String phoneNo;

    public Student(String name, int marks, String phoneNo) {
        this.name = name;
        this.marks = marks;
        this.phoneNo = phoneNo;
    }

    @Override
    public String toString() {

        return
                ", name=" + name +
                        ", marks=" + marks +
                        ", phoneNo=" + phoneNo +
                        "\n";
    }
}
