package com.tw.studentData;

import java.util.Scanner;

public class PrimarySchool {

    public static void main(String[] args) {

        StudentRecord studentRecord = new StudentRecord();

        String choice = "Yes";

        Scanner input = new Scanner(System.in);

        while (choice.equals("Yes")) {

            System.out.println("role");
            String role = input.next();

            if (role.equals("admin")) {

                System.out.println("operation");
                String operation = input.next();

                switch (operation) {
                    case "add":
                        addStudent(studentRecord, input);
                        break;
                    case "remove":
                        removeStudent(studentRecord, input);
                        break;
                    case "update":
                        updateStudent(studentRecord, input);
                        break;
                    default:
                        break;
                }
            }
            if (role.equals("staff")) {
                System.out.println(studentRecord.getStudentData());
            }
            if (!role.equals("admin") && !role.equals("staff"))
                break;

            System.out.println("Want to continue [Yes/No]");
            choice = input.next();
        }
    }

    private static void removeStudent(StudentRecord studentRecord, Scanner input) {
        System.out.println("rollToBeRemoved");

        int rollToBeRemoved = input.nextInt();
        studentRecord.removeStudent(rollToBeRemoved);
        System.out.println(studentRecord.getStudentData());
    }

    private static void updateStudent(StudentRecord studentRecord, Scanner scanner) {
        System.out.println("enter rollno name marks phoneNo");

        studentRecord.updateStudent(scanner.nextInt(), new Student(scanner.next(), scanner.nextInt(), scanner.next()));
        System.out.println(studentRecord.getStudentData());
    }

    private static void addStudent(StudentRecord studentRecord, Scanner scanner) {
        System.out.println("enter rollno name marks phoneNo");

        studentRecord.addStudent(scanner.nextInt(), new Student(scanner.next(), scanner.nextInt(), scanner.next()));
        System.out.println(studentRecord.getStudentData());
    }
}
